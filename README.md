### Ad Removal for NodeCore

...cleanly removes every ad from NodeCore, collapsing the space they occupied.

...has no side effects and can safely be disabled/reenabled, or removed, at any time.

...is *not* an advertisement removal mod.  It only removes ads, and will not be expanded to include removal of vertisements.

...may *break translations*.  It is not recommended on public servers, or anywhere else non-English language options may be used.