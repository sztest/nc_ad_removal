-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs, string, type
    = minetest, nodecore, pairs, string, type
local string_gsub, string_sub, string_upper
    = string.gsub, string.sub, string.upper
-- LUALOCALS > ---------------------------------------------------------

local function adremove(s)
	if type(s) ~= "string" then return s end
	s = string_gsub(s, "Ad(%w)", string_upper)
	return string_gsub(s, "[Aa][Dd]", "")
end

-- Hint text

minetest.after(0, function()
		for _, v in pairs(nodecore.hints) do
			v.text = adremove(v.text)
		end
	end)

-- Registered item descriptions

nodecore.register_on_register_item({
		func = function(_, def)
			if not def.description then return end
			def.description = adremove(def.description)
		end,
		retroactive = true
	})

-- Player guide

do
	local function tabwrap(content)
		if type(content) == "function" then
			return function(...)
				return tabwrap(content(...))
			end
		end
		if type(content) == "table" then
			for k, v in pairs(content) do
				content[k] = tabwrap(v)
			end
		end
		if type(content) == "string" then
			return adremove(content)
		end
		return content
	end
	minetest.after(0, function()
			for _, tab in pairs(nodecore.registered_inventory_tabs) do
				if tab.content and not tab.raw then
					tab.content = tabwrap(tab.content)
				end
			end
		end)
end

-- Chat messages

minetest.after(0, function()
		do
			local old = minetest.chat_send_all
			function minetest.chat_send_all(text, ...)
				old(adremove(text), ...)
			end
		end
		do
			local old = minetest.chat_send_player
			local prefold = "DM from "
			local prefnew = minetest.translate("__builtin", "DM from XXX")
			:gsub("XXX.*", "")
			function minetest.chat_send_player(name, text, ...)
				if string_sub(text, 1, #prefnew) == prefnew
				or string_sub(text, 1, #prefold) == prefold then
					return old(name, text, ...)
				end
				old(name, adremove(text), ...)
			end
		end
	end)
