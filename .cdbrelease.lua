-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "nc_ad_removal",
	version = stamp .. "-$Format:%h$",
	type = "mod",
	dev_state = "MAINTENANCE_ONLY",
	title = "NodeCore Ad Removal",
	short_description = "Removes all ads from NodeCore",
	tags = {"april_fools", "gui", "hud", "singleplayer"},
	license = "MIT",
	media_license = "MIT",
	repo = "https://gitlab.com/sztest/nc_ad_removal",
	issue_tracker = "https://discord.gg/NNYeF6f",
	long_description = readtext('README.md'),
	screenshots = {readbinary(".cdb-1.webp"), readbinary(".cdb-2.webp")}
}

-- luacheck: pop
